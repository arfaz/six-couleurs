package View;

import Controller.MenuController;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;


public class ComputerMenu{

	private MenuController mc;
	private int nbPlayer;
	
	public ComputerMenu(MenuController m, int n){
		this.mc = m;
		this.nbPlayer = n;
	}
	
	// Initialise l'interface utilisateur
	public void initUI(Stage s){
	        s.setTitle("Computer Menu");
	        s.setScene(this.getLayout());
			s.show();
	}
	
	// Construit la scene graphique
	private Scene getLayout(){
		// Panel contenant les boutons
		
		Group g = new Group();
		
		Scene s = new Scene(g, 650,650, Color.BLACK);
		
		FlowPane fp = new FlowPane();
		fp.setPrefSize(500, 400);
		fp.setLayoutX(100);
		fp.setLayoutY(250);
		fp.setHgap(70);
		fp.setVgap(70);
				
		// Cr�ation des boutons
		for(int i =0; i<= 4-this.nbPlayer; i++){
			if(this.nbPlayer==1 && i==0){
				continue;
			}
			
			fp.getChildren().add(this.initButton(i+""));
		}
		
		g.getChildren().add(fp);
		
		return s;
	}
	
	// Cr�er les boutons
	private Button initButton(String text){
		Button b = new Button(text +" Computer");
		b.setAccessibleText(text);
		// Taille du bouton
		b.setPrefSize(200, 100);
		
		// Listener onClick
		b.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
		    this.mc.createPlayers(Integer.parseInt(b.getAccessibleText()));
		});
		
		return b;
	}
}
