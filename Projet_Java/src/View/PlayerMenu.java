package View;

import Controller.MenuController;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;


public class PlayerMenu{

	private MenuController mc;
	
	
	public PlayerMenu(MenuController m){
		this.mc = m;
	}
	
	// Initialise l'interface utilisateur
	public void initUI(Stage s){
	        s.setTitle("Player Menu");
	        s.setScene(this.getLayout());
			s.show();
	}
	
	// Construit la scene graphique
	private Scene getLayout(){
		// Panel contenant les boutons
		
		Group g = new Group();
		
		Scene s = new Scene(g, 650,650, Color.BLACK);
		
		FlowPane fp = new FlowPane();
		fp.setPrefSize(500, 400);
		fp.setLayoutX(100);
		fp.setLayoutY(250);
		fp.setHgap(70);
		fp.setVgap(70);
				
		// Cr�ation des boutons
		Button b1 = this.initButton("1 Player");
		b1.setAccessibleText("1");
				
		Button b2 = this.initButton("2 Players");
		b2.setAccessibleText("2");
		
		Button b3 = this.initButton("3 Players");
		b3.setAccessibleText("3");
		
		Button b4 = this.initButton("4 Players");
		b4.setAccessibleText("4");
		
		// Ajout des boutons au panel
		fp.getChildren().add(b1);
		fp.getChildren().add(b2);
		fp.getChildren().add(b3);
		fp.getChildren().add(b4);
		
		g.getChildren().add(fp);
		
		return s;
	}
	
	// Cr�er les boutons
	private Button initButton(String text){
		Button b = new Button(text);
		
		// Taille du bouton
		b.setPrefSize(200, 100);
		
		// Listener onClick
		b.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
		    this.mc.nbComputerChoice(Integer.parseInt(b.getAccessibleText()));
		});
		
		return b;
	}
}
