package View;

import java.text.DecimalFormat;
import java.util.ArrayList;
import Controller.GameController;
import Model.Game;
import Model.Player;
import Model.Map.Map;
import Pattern.Observer;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

public class GameView implements Observer{

	private GameController gc;
	private static int cursor;
	
	private static final int WINDOW_WIDTH = 650;
	private static final int WINDOW_HEIGHT = 650;
	private int gridWidth = WINDOW_WIDTH;
	private int gridHeight = WINDOW_HEIGHT;
	
	public GameView(GameController g){
		this.gc=g;
		GameView.cursor=0;
		this.gridWidth = WINDOW_WIDTH;
		this.gridHeight = WINDOW_HEIGHT;
	}
	
	public static void incrementCursor(int nbPlayers){
		GameView.cursor = (GameView.cursor+1)% nbPlayers;
	}
	
	public static int getCursor(){
		return GameView.cursor;
	}
	
	public void initUI(Stage s){
		this.gridWidth = WINDOW_WIDTH;
		this.gridHeight = WINDOW_HEIGHT;
        s.setTitle("Filler Game (" + Game.getLevel(this.gc.getGame().getLevel()) + ")" );
        s.setScene(this.getLayout());
		s.show();
	}
	
	// Create the Content
	private Scene getLayout(){
		Group g = new Group();
		
		Scene s = new Scene(g, GameView.WINDOW_WIDTH,GameView.WINDOW_HEIGHT, Color.BLACK);
		
		BorderPane bp = new BorderPane();
		bp.setPrefSize(GameView.WINDOW_WIDTH, GameView.WINDOW_HEIGHT);
		
		BorderPane bpContent = new BorderPane();
		
		bpContent.setTop(this.topPane());
		bpContent.setBottom(this.bottomPane(this.gc.getGame().getPlayers()));
		bpContent.setCenter(this.centerPane(this.gc.getGame().getMap()));
		
		MenuBar menuBar = new MenuBar();
		 
        // --- Menu
        Menu menuFile = new Menu("File");
        
        // --- MenuItem
        MenuItem newGame = new MenuItem("New Game");
              
        newGame.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                gc.newGame();
            }
        });
 
        // --- MenuItem
        MenuItem save = new MenuItem("Save...");
              
        save.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                gc.save();
            }
        });
        
        // --- MenuItem
        MenuItem quit = new MenuItem("Quit");
              
        quit.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                gc.quit();
            }
        });
        
        menuFile.getItems().addAll(newGame,save,quit);
        menuBar.getMenus().addAll(menuFile);

        bp.setCenter(bpContent);
        bp.setTop(menuBar);
		
        g.getChildren().add(bp);
		return s;
	}
	
	
	// Crate topPane
	private HBox topPane(){
		HBox hb = new HBox();
		hb.setPrefSize(GameView.WINDOW_WIDTH, 80);
		hb.setStyle("-fx-border-color:grey;-fx-border-width:2px");
		
		this.gridHeight -= hb.getPrefHeight();
		
		for(Player p : this.gc.getGame().getPlayers()){
			
			Label lb = new Label(p.getName()+ " : " + new DecimalFormat("#.0").format(p.getScore()/(double)(this.gc.getGame().getMap().getNbCell())*100) +"%");
			lb.setPrefSize(120, 50);
			lb.setTextAlignment(TextAlignment.CENTER);
			
			String color = p.getColor().toString().substring(2);
			
			if(this.gc.getGame().getCurrent() == p){
				lb.setStyle("-fx-background-color:#"+color+";-fx-border-color:white;-fx-border-width:2px;");
			}
			
			else{
				lb.setStyle("-fx-background-color:#"+color);
			}
			hb.setSpacing(30);
			hb.setPadding(new Insets(10,50,10,50));
			hb.getChildren().add(lb);
		}
		return hb;
	}
	
	// Create TopPane Content
	
	
	//Create BottomPane
	private HBox bottomPane(ArrayList<Player> players){
	
		HBox hb = new HBox();
		hb.setPrefSize(GameView.WINDOW_WIDTH, 121);
		
		this.gridHeight -= hb.getPrefHeight();
		
		for(Button b : this.generateColorButtons()){
			hb.getChildren().add(b);
		}
		
		hb.setPadding(new Insets(20,0,0,95));
		hb.setSpacing(25);
		hb.setStyle("-fx-border-color:grey;-fx-border-width:2px");
		return hb;
	}
	
	//Create Bottom Paze Content
	private ArrayList<Button> generateColorButtons(){
		ArrayList<Button> buttons = new ArrayList<Button>();
		
		for(int i = 1 ; i<=Game.getColors().size(); ++i){
			
			Rectangle r = new Rectangle(40,40, Game.getColors().get(i));
			Button b = new Button();
			
			b.setGraphic(r);
			b.setPrefSize(30, 30);
			b.setAccessibleText(i+"");
			
			for(Player p: this.gc.getGame().getPlayers()){
				if(Game.getColors().get(i) == p.getColor()){
					b.setDisable(true);
				}
			}
			
			b.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
			    this.gc.play(cursor,Integer.parseInt(b.getAccessibleText()));
			    GameView.incrementCursor(this.gc.getGame().getPlayers().size());
			});
			
			buttons.add(b);
		}
		
		return buttons;
	}
	
	
	// Create Center PAne
	private GridPane centerPane(Map m){
		GridPane gp = new GridPane();
		gp.setStyle("-fx-border-color:grey;-fx-border-width:2px");
		
		// Calculate the width and height of rectangle to draw squares
		double rectangleWidth = ((this.gridWidth*0.8)/m.getWidth()) - 2;
		double rectangleHeight = this.gridHeight/m.getHeight() - 2;
		
		if(rectangleWidth < rectangleHeight){
			rectangleHeight = rectangleWidth;
		}
		else{
			rectangleWidth = rectangleHeight;
		}
		
		
		// Set color on each grid cell
		for(int i = 0 ; i<m.getGrille().length ; i++){
			for(int j = 0 ; j<m.getGrille()[i].length ; j++){
				
				Color c=null;
				Label lb = null;
				if(m.getGrille()[i][j] instanceof Integer){
					c = Game.getColors().get(m.getGrille()[i][j]);
				}
				
				else if(m.getGrille()[i][j] instanceof Player){
					c = ((Player) (m.getGrille()[i][j])).getColor();
					lb = new Label(((Player) (m.getGrille()[i][j])).getShortName());
				}
				
				Rectangle r = new Rectangle(rectangleWidth, rectangleHeight,c);
				r.setArcHeight(10);
				r.setArcWidth(10);
				
		        GridPane.setHalignment(r, HPos.CENTER);
		        gp.add(r, i, j);
		        
		        if(lb!=null){
		        	GridPane.setHalignment(lb, HPos.CENTER);
		        	gp.add(lb, i, j);
		        }
			}
		}
        
		gp.setHgap(2);
		gp.setVgap(2);
		
		// Calculate padding to center the gridPane
		double leftSpacing = (this.gridWidth - (m.getWidth()* rectangleWidth) - (gp.getHgap() * (m.getWidth()-1))) /2;
		double topSpacing  = (this.gridHeight - (m.getHeight()* rectangleHeight) - (gp.getVgap() * (m.getHeight()-1))) /2.5;
		
		
		gp.setPadding(new Insets(Math.max(0, topSpacing),0,0, Math.max(0, leftSpacing)));
		
		
		return gp;
	}
	
		
	@Override
	public void update() {
		this.initUI(this.gc.getStage());
	}

}
