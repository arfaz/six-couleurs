package View;


import Controller.MenuController;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class MapMenu{

	private MenuController mc;
	
	public MapMenu(MenuController m){
		this.mc=m;
	}
	
	public void initUI(Stage s){
	        s.setTitle("Map Choice Menu");
	        s.setScene(this.getLayout());
			s.show();
	}
	
	private Scene getLayout(){
		Group g = new Group();
		
		Scene s = new Scene(g, 650,650, Color.BLACK);
		
		// Panel contenant les boutons
		FlowPane fp = new FlowPane();
		fp.setPrefSize(500, 400);
		fp.setLayoutX(100);
		fp.setLayoutY(250);
		fp.setHgap(70);
		fp.setVgap(70);
				
		// Cr�ation des boutons
		Image image = new Image(getClass().getResourceAsStream("../resources/carre.png"));
		Button b1 = this.initButton(new ImageView(image));
		b1.setAccessibleText("square");
				
		image = new Image(getClass().getResourceAsStream("../resources/diamond.png"));
		Button b2 = this.initButton(new ImageView(image));
		b2.setAccessibleText("diamond");
		
		image = new Image(getClass().getResourceAsStream("../resources/triangle.png"));
		Button b3 = this.initButton(new ImageView(image));
		b3.setAccessibleText("triangle");
		
		image = new Image(getClass().getResourceAsStream("../resources/hexa.png"));
		Button b4 = this.initButton(new ImageView(image));
		b4.setAccessibleText("hexa");
		
		// Ajout des boutons au panel
		fp.getChildren().add(b1);
		fp.getChildren().add(b2);
		fp.getChildren().add(b3);
		fp.getChildren().add(b4);
				
		// Ajout du panel au contenu
		g.getChildren().add(fp);
		
		return s;
	}
	
	private Button initButton(ImageView iv){
		Button b = new Button();
		b.setGraphic(iv);
		
		// Taille du bouton
		b.setPrefSize(200, 100);
		
		// Listener onClick
		b.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
		    this.mc.createMap(b.getAccessibleText());
		});
		
		return b;
	}
}
