package View;

import Controller.GameController;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class EndGameView {

private GameController gc;
	
	
	public EndGameView(GameController g){
		this.gc = g;
	}
	
	// Initialise l'interface utilisateur
	public void initUI(Stage s){
	        s.setTitle("END GAME");
	        s.setScene(this.getLayout());
			s.show();
	}
	
	// Construit la scene graphique
	private Scene getLayout(){
		// Panel contenant les boutons
		
		Group g = new Group();
		
		Scene s = new Scene(g, 650,650, Color.GREY);
		
		StackPane sp = new StackPane();
				
		// Cr�ation des boutons
		Label lb1 = new Label("Fin Du Jeu");
		lb1.setScaleX(2.5);
		lb1.setScaleY(2.5);
		lb1.setTranslateX(275);
		lb1.setTranslateY(70);
		
		sp.getChildren().add(lb1);

		Label lbWinnerMsg = new Label("");
		lbWinnerMsg.setScaleX(2.5);
		lbWinnerMsg.setScaleY(2.5);
		lbWinnerMsg.setTranslateX(280);
		lbWinnerMsg.setTranslateY(170);

		if(this.gc.getGame().getBestPlayer().size() == 1){
			lbWinnerMsg.setText("Le gagnant est : ");
			
			
			Label lbWinnerName = new Label(this.gc.getGame().getBestPlayer().get(0).getName());
			lbWinnerName.setScaleX(2.5);
			lbWinnerName.setScaleY(2.5);
			lbWinnerName.setTranslateX(270);
			lbWinnerName.setTranslateY(250);
			
			sp.getChildren().add(lbWinnerName);
		}
		
		else{
			lbWinnerMsg.setText("Egalit� entre : ");
			for(int i = 0; i<this.gc.getGame().getBestPlayer().size(); i++){
				Label lbWinnerName = new Label(this.gc.getGame().getBestPlayer().get(i).getName());
				lbWinnerName.setScaleX(2.5);
				lbWinnerName.setScaleY(2.5);
				lbWinnerName.setTranslateX(270);
				lbWinnerName.setTranslateY(250+i*50);
				
				
				sp.getChildren().add(lbWinnerName);
			}
		}
		
		Button replay = new Button("Rejouer");
		replay.setPrefSize(100, 70);
		replay.setTranslateX(270);
		replay.setTranslateY(370);
		
		replay.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
		    this.gc.newGame();
		});
		
		sp.getChildren().add(lbWinnerMsg);
		sp.getChildren().add(replay);
		
		g.getChildren().add(sp);
		
		return s;
	}
	
	
}
