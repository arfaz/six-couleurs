package Utils;

import Model.Map.Map;
import javafx.util.Pair;

public class MapAnalyze {

	private Map map;
	
	public MapAnalyze(Map m){
		this.map=m;
	}
	
	public boolean checkRightCellExist(Pair<Integer, Integer> c, int width){
		return (c.getKey()+1 >=0 && c.getKey()+1 < width);
	}
	
	public boolean checkLeftCellExist(Pair<Integer, Integer> c ,int width){
		return (c.getKey()-1 >=0 && c.getKey()-1 < width);
	}
	
	public boolean checkTopCellExist(Pair<Integer, Integer> c, int height){
		return (c.getValue()-1 >=0 && c.getValue()-1 < height);
	}
	
	public boolean checkBottomCellExist(Pair<Integer, Integer> c ,int height){
		return (c.getValue()+1 >=0 && c.getValue()+1 < height);
	}
	
	
	public boolean isUselessCell(Pair<Integer,Integer> cell){
		int sameColorCell=0;
		Object[][] grille = this.map.getGrille();
		
		if(!this.checkRightCellExist(cell, this.map.getWidth()) 
				|| grille[cell.getKey()+1][cell.getValue()] == null
				|| grille[cell.getKey()+1][cell.getValue()] == grille[cell.getKey()][cell.getValue()]){
			
			sameColorCell++;
		}
		
		if(!this.checkLeftCellExist(cell, this.map.getWidth()) 
				|| grille[cell.getKey()-1][cell.getValue()] == null
				|| grille[cell.getKey()-1][cell.getValue()] == grille[cell.getKey()][cell.getValue()]){
			
			sameColorCell++;
		}
		
		if(!this.checkTopCellExist(cell, this.map.getHeight()) 
				|| grille[cell.getKey()][cell.getValue()-1] == null
				|| grille[cell.getKey()][cell.getValue()-1] == grille[cell.getKey()][cell.getValue()]){
			
			sameColorCell++;
		}
		
		if(!this.checkBottomCellExist(cell, this.map.getHeight()) 
				|| grille[cell.getKey()][cell.getValue()+1] == null
				|| grille[cell.getKey()][cell.getValue()+1] == grille[cell.getKey()][cell.getValue()]){
			
			sameColorCell++;
		}
		
		return sameColorCell == 4;
	}
	
}
