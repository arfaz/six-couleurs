package Model;

import java.util.ArrayList;
import java.util.HashMap;

import Model.Map.Map;
import Pattern.*;
import javafx.scene.paint.Color;

public class Game extends AbstractObservable {
	
	private static HashMap<Integer, Color> colors;
	private static HashMap<Integer, String> levels;
	private ArrayList<Player> players;
	private Player current;
	private Map map;
	private int level;
	
	public Game(){
		super();
		
		colors = new HashMap<Integer, Color>();
		colors.put(1, Color.DARKGRAY);
		colors.put(2, Color.DEEPSKYBLUE);
		colors.put(3, Color.RED);
		colors.put(4, Color.LIMEGREEN);
		colors.put(5, Color.YELLOW);
		colors.put(6, Color.BLUEVIOLET);
		
		levels = new HashMap<Integer, String>();
		levels.put(1, "EASY");
		levels.put(2, "MEDIUM");
		levels.put(3, "HARD");
		levels.put(4, "MASTER");
	}
	
	public Game(ArrayList<Player> p, Map m){
		this();
		this.players=p;
		this.map=m;
	}
	
	public ArrayList<Player> getPlayers(){
		return this.players;
	}
	
	public void setPlayer(ArrayList<Player> p){
		this.players=p;
	}
	
	public Map getMap(){
		return this.map;
	}
	
	public void setMap(Map m){
		this.map=m;
	}
	
	public int getLevel(){
		return this.level;
	}
	
	public void setLevel(int l){
		this.level=l;
	}
	
	public static HashMap<Integer, Color> getColors(){
		return colors;
	}
	
	public static String getLevel(int i){
		return levels.get(i);
	}

	public Player getCurrent() {
		return current;
	}

	public void setCurrent(Player current) {
		this.current = current;
		this.notifyObserver();
	}
	
	public ArrayList<Player> getBestPlayer(){
		ArrayList<Player> best = new ArrayList<Player>();
		best.add(this.getPlayers().get(0));
		
		for(int i =1; i<this.getPlayers().size(); i++){ 
			if(this.getPlayers().get(i).getScore()> best.get(0).getScore()){
				best.clear();
				best.add(this.getPlayers().get(i));
			}
			
			else if(this.getPlayers().get(i).getScore()== best.get(0).getScore()){
				best.add(this.getPlayers().get(i));
			}
		}
		return best;
	}
	
	private int gettableCells(){
		int mapCells =  this.getMap().getNbCell();
		
		for(Player p: this.getPlayers()){
			mapCells -= p.getScore();
		}
		
		return mapCells;
	}
	
	public boolean isEndOfGame(){
		ArrayList<Player> best = this.getBestPlayer();
		boolean end = true;
		
		int gettableCells = this.gettableCells();
		int i=0;
				
		while(i<this.getPlayers().size() && end){
			if(best.get(0) != this.getPlayers().get(i) && (this.getPlayers().get(i).getScore()+gettableCells) >= best.get(0).getScore() && gettableCells >0){
				end =false;
			}
			
			i++;
		}
		
		return end;
	}

}
