package Model;

import Utils.MapAnalyze;
import javafx.scene.paint.Color;
import javafx.util.Pair;

public class ComputerMedium extends Computer {

	public ComputerMedium(int n, Color c, Game g, MapAnalyze m) {
		super(n, c,g,m);
		// TODO Auto-generated constructor stub
	}

	public int play(){
		Object[][] grille = this.getGame().getMap().getGrille();
		int col = -1;
		Pair<Integer, Integer> cell;
		
		int i=0;
		
		while(i<this.getGame().getPlayers().size()){
			Player p= this.getGame().getPlayers().get(i);
			
				int j=p.getCellList().size()-1;
				
				// Tant que la couleur choisie n'est pas disponible 
				while(j>=0 && col ==-1){
					cell = p.getCellList().get(j);
					
					
					
					
					// Si une des case adjacente 
					if(this.getMapAn().checkRightCellExist(cell, this.getGame().getMap().getWidth()) 
							&& grille[cell.getKey()+1][cell.getValue()] !=null
							&& !(grille[cell.getKey()+1][cell.getValue()] instanceof Player)
							&& Game.getColors().get(grille[cell.getKey()+1][cell.getValue()]) != p.getColor()
							&& this.isValidColor((int)grille[cell.getKey()+1][cell.getValue()])){
						
						col = (int)grille[cell.getKey()+1][cell.getValue()];
					}
					
					else if(this.getMapAn().checkLeftCellExist(cell, this.getGame().getMap().getWidth()) 
							&& grille[cell.getKey()-1][cell.getValue()] !=null
							&& !(grille[cell.getKey()-1][cell.getValue()] instanceof Player)
							&& Game.getColors().get(grille[cell.getKey()-1][cell.getValue()]) != p.getColor()
							&& this.isValidColor((int)grille[cell.getKey()-1][cell.getValue()])){
						
						col = (int)grille[cell.getKey()-1][cell.getValue()];
					}
					
					else if(this.getMapAn().checkBottomCellExist(cell, this.getGame().getMap().getHeight()) 
							&& grille[cell.getKey()][cell.getValue()+1] !=null
							&& !(grille[cell.getKey()][cell.getValue()+1] instanceof Player)
							&& Game.getColors().get(grille[cell.getKey()][cell.getValue()+1]) != p.getColor()
							&& this.isValidColor((int)grille[cell.getKey()][cell.getValue()+1])){
						
						col = (int)grille[cell.getKey()][cell.getValue()+1];
					}
					
					else if(this.getMapAn().checkTopCellExist(cell, this.getGame().getMap().getHeight()) 
							&& grille[cell.getKey()][cell.getValue()-1] !=null
							&& !(grille[cell.getKey()][cell.getValue()-1] instanceof Player)
							&& Game.getColors().get(grille[cell.getKey()][cell.getValue()-1]) != p.getColor()
							&& this.isValidColor((int)grille[cell.getKey()][cell.getValue()-1])){
						
						col = (int)grille[cell.getKey()][cell.getValue()-1];
					}
					
					j--;
				
			}
			
			i++;
		}
		
		if(col==-1){
			col = this.playEasyIA();
		}
		
		return col;
	}
	
}
