package Model;

import javafx.scene.paint.Color;
import javafx.util.Pair;

import java.util.ArrayList;

public class Player {
	
	private String name;
	private String shortName;
	private Color color;
	private int score;
	
	private ArrayList<Pair<Integer,Integer>> cellList;
		
	public Player(String n, Color c)
	{
		this.name=n;
		this.color=c;
		this.score=1;
		this.cellList = new ArrayList<Pair<Integer,Integer>>();
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public Color getColor()
	{
		return this.color;
	}
	
	public void setColor(Color c)
	{
		this.color=c;
	}
	
	public int getScore()
	{
		return this.score;
	}
	
	public void addScore(int s)
	{
		this.score += s;
	}
	
	public ArrayList<Pair<Integer,Integer>> getCellList() {
		return cellList;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	
}
