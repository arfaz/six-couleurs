package Model;

import java.util.ArrayList;
import java.util.Comparator;

import Utils.MapAnalyze;
import javafx.scene.paint.Color;
import javafx.util.Pair;

public class ComputerHard extends Computer {
	
	public ComputerHard(int n, Color c, Game g, MapAnalyze m) {
		super(n, c,g,m);
	}
	
	public int play(){
		int score;
		
		ArrayList<Pair<Integer,Integer>> colorScoreList = new ArrayList<Pair<Integer,Integer>>();
		ArrayList<Pair<Integer,Integer>> cellListCopy ;
		
		for(int playedColor=1; playedColor<= Game.getColors().size(); playedColor++){
			
			if(this.isValidColor(playedColor)){
				cellListCopy = new ArrayList<Pair<Integer,Integer>>(this.getGame().getCurrent().getCellList());
				score= this.simulatePlay(this.getGame().getCurrent(), Game.getColors().get(playedColor), cellListCopy, this.getGame().getMap().getGrilleCopy(), 0,0);
				colorScoreList.add(new Pair<Integer,Integer>(playedColor,score));
			}
		}
		
		colorScoreList.sort(new Comparator<Pair<Integer, Integer>>() {
	        @Override
	        public int compare(Pair<Integer, Integer> score1, Pair<Integer, Integer> score2) {
	            if (score1.getValue() > score2.getValue()) {
	                return -1;
	            } else if (score1.getValue().equals(score2.getValue())) {
	                return 0; // You can change this to make it then look at the
	                          //words alphabetical order
	            } else {
	                return 1;
	            }
	        }
	    });
		
		return colorScoreList.get(0).getKey();
	}
	
	public int simulatePlay(Player p, Color c , ArrayList<Pair<Integer,Integer>> cellListCopy, Object[][] g, int indice, int colorOccurence){
		Object[][] grille = g;
		Pair<Integer, Integer> cell;
		
		
		if(cellListCopy.isEmpty() || indice==cellListCopy.size()){
			
			return colorOccurence;
		}
		
		else{
			cell=cellListCopy.get(indice);
		}
		
		
		if(this.getMapAn().checkRightCellExist(cell, this.getGame().getMap().getWidth()) 
				&& grille[cell.getKey()+1][cell.getValue()] !=null
				&& Game.getColors().get(grille[cell.getKey()+1][cell.getValue()]) == c){
			
			grille[cell.getKey()+1][cell.getValue()] = p;
			
			cellListCopy.add(new Pair<Integer,Integer>(cell.getKey()+1,cell.getValue()));
			colorOccurence++;
		}
		
		if(this.getMapAn().checkLeftCellExist(cell, this.getGame().getMap().getWidth()) 
				&& grille[cell.getKey()-1][cell.getValue()] !=null
				&& Game.getColors().get(grille[cell.getKey()-1][cell.getValue()]) == c){
			
			grille[cell.getKey()-1][cell.getValue()] = p;
			
			cellListCopy.add(new Pair<Integer,Integer>(cell.getKey()-1,cell.getValue()));
			colorOccurence++;
		}

		if(this.getMapAn().checkTopCellExist(cell, this.getGame().getMap().getHeight())
				&& grille[cell.getKey()][cell.getValue()-1] !=null
				&& Game.getColors().get(grille[cell.getKey()][cell.getValue()-1]) == c){
			
			grille[cell.getKey()][cell.getValue()-1] = p;
			
			cellListCopy.add(new Pair<Integer,Integer>(cell.getKey(),cell.getValue()-1));
			colorOccurence++;
		}
		
		if(this.getMapAn().checkBottomCellExist(cell, this.getGame().getMap().getHeight())
				&& grille[cell.getKey()][cell.getValue()+1] !=null
				&& Game.getColors().get(grille[cell.getKey()][cell.getValue()+1]) == c){
			
			grille[cell.getKey()][cell.getValue()+1] = p;
			
			cellListCopy.add(new Pair<Integer,Integer>(cell.getKey(),cell.getValue()+1));
			colorOccurence++;
		}
		
		
		if(this.getMapAn().isUselessCell(cell)){
			cellListCopy.remove(indice);
		}
		
		else{
			indice++;
		}
		
		return this.simulatePlay(p, c, cellListCopy, grille, indice, colorOccurence);
	}
	
	
}
