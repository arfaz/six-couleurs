package Model;

import java.util.Random;

import Utils.MapAnalyze;
import javafx.scene.paint.Color;

public abstract class Computer extends Player{

	private Game game;
	private MapAnalyze mapAn;
	
	public Computer(int n, Color c, Game g, MapAnalyze m) {
		super("Computer "+n, c);
		this.game=g;
		this.mapAn= m;
		// TODO Auto-generated constructor stub
	}

	public abstract int play();
	
	public boolean isValidColor(int col){
		int count = 0;

		
		if(col <0){
			return false;
		}
		
		for(Player p : this.game.getPlayers()){
			if(Game.getColors().get(col) == p.getColor() ){
				count++;
			}
		}
		
		return count==0;
	}
	
	public int playEasyIA(){
		Random r = new Random();
		
		int col = r.nextInt(6)+1;
		
		while(!isValidColor(col)){
			col = r.nextInt(6)+1;
		}
		
		return col;
	}

	public MapAnalyze getMapAn() {
		return mapAn;
	}
	
	public Game getGame() {
		return game;
	}
	
	

}
