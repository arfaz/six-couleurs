package Model.Map;

import java.util.ArrayList;
import java.util.Random;

import Model.Player;
import javafx.util.Pair;

public class DiamondMap extends Map {

	public DiamondMap(){
		super(9);
	}
	
	@Override
	public void initGrille() {
		Random r = new Random();
		Object[][] g = new Object[this.getWidth()][this.getHeight()];
		int startX, endX;
		
		for(int j =0; j<this.getHeight(); ++j){
			if(j<=this.getHeight()/2){
				startX= ((this.getWidth()/2) - j);
				endX = ((this.getWidth()/2) + j);
			}
			else{
				startX= ((this.getWidth()/2) - (this.getHeight()-1-j));
				endX = ((this.getWidth()/2) + (this.getHeight()-1-j));
			}
			
			for(int i =startX; i<=endX; ++i){
				
				g[i][j]=(Integer)r.nextInt(6)+1;
				this.setNbCell(1);
				
			}
		}
		this.setGrille(g);
	}


	@Override
	protected void setWidthAndHeight(int s) {
		// TODO Auto-generated method stub
		this.setWidth((s*2)-1);
		this.setHeight((s*2)-1);
	}

	@Override
	public void initPlayerPosition(ArrayList<Player> pList) {
		// TODO Auto-generated method stub
		
		this.getGrille()[0][this.getHeight()/2] = pList.get(0);
		pList.get(0).getCellList().add(new Pair<Integer, Integer>(0,this.getHeight()/2));
		
		this.getGrille()[this.getWidth()-1][this.getHeight()/2] = pList.get(1);
		pList.get(1).getCellList().add(new Pair<Integer, Integer>(this.getWidth()-1,this.getHeight()/2));
		
		if(pList.size() == 3){
			this.getGrille()[this.getWidth()/2][this.getHeight()/2] = pList.get(2);
			pList.get(2).getCellList().add(new Pair<Integer, Integer>(this.getWidth()/2,this.getHeight()/2));
		}
		
		else if(pList.size() == 4){
			this.getGrille()[this.getWidth()/2][0] = pList.get(2);
			pList.get(2).getCellList().add(new Pair<Integer, Integer>(this.getWidth()/2,0));
			
			this.getGrille()[this.getWidth()/2][this.getHeight()-1] = pList.get(3);
			pList.get(3).getCellList().add(new Pair<Integer, Integer>(this.getWidth()/2,this.getHeight()-1));
		}
		
	}

}
