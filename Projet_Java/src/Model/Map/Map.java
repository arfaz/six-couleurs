package Model.Map;

import java.util.ArrayList;
import java.util.Random;

import Model.Player;
import Pattern.*;

public abstract class Map extends AbstractObservable {
	
	private int width;
	private int height;
	private int nbCell;
	private Object[][] grille;
		
	public Map(int s){
		this.setWidthAndHeight(s);
		this.nbCell = 0;
		this.initGrille();
		
	}
	
	protected abstract void setWidthAndHeight(int s);
	
	
	public void setGrille(Object[][] g){
		this.grille=g;
	}
	
	public Object[][] getGrille(){
		return this.grille;
	}
	
	public abstract void initGrille();

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}
	
	public Object[][] getGrilleCopy(){
		
		Object[][] copyGrille = new Object[this.width][this.height];
		
		for(int i=0; i<this.width;i++){
			for(int j=0; j<this.height; j++){
				copyGrille[i][j] = this.getGrille()[i][j];
			}
		}
		
		return copyGrille;
	}
	
	public void createObstacle(int gameLevel){
		Random rand = new Random();
		int i,j;
		
		int nbObstacleMax=(int)((this.getHeight()*this.getWidth())*(gameLevel/75.0));
		
		if(this instanceof TriangleMap){
			nbObstacleMax/=2.5;
		}
		
		for(int obstacle = 0; obstacle<nbObstacleMax; obstacle++){
			i = rand.nextInt(this.width);
			j = rand.nextInt(this.height);
			
			while(this.getGrille()[i][j] == null || this.getGrille()[i][j] instanceof Player){
				i = rand.nextInt(this.width);
				j = rand.nextInt(this.height);
			}
			this.nbCell--;
			this.grille[i][j] = null;
		}
	}
	
	public abstract void initPlayerPosition(ArrayList<Player> pList);
	
	public int getNbCell(){
		return this.nbCell;
	}
	
	public void setNbCell(int nb){
		this.nbCell += nb;
	}
	
	
}
