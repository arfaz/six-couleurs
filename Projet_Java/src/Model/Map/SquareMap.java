package Model.Map;

import java.util.ArrayList;
import java.util.Random;

import Model.Player;
import javafx.util.Pair;

public class SquareMap extends Map{

	public SquareMap(){
		super(13);
		this.setNbCell(this.getHeight()*this.getWidth());
	}
	
	@Override
	public void initGrille() {
		Random r = new Random();
		Object[][] g = new Object[this.getWidth()][this.getHeight()];
		
		for(int j =0; j<this.getHeight(); ++j){
			for(int i =0; i<this.getWidth(); ++i){
				
				g[i][j]=(Integer)r.nextInt(6)+1;
				
			}
		}
		this.setGrille(g);
	}

	@Override
	protected void setWidthAndHeight(int s) {
		// TODO Auto-generated method stub
		this.setHeight(s);
		this.setWidth(s);
	}

	@Override
	public void initPlayerPosition(ArrayList<Player> pList) {
		// TODO Auto-generated method stub
		
		this.getGrille()[0][this.getHeight()-1] = pList.get(0);
		pList.get(0).getCellList().add(new Pair<Integer, Integer>(0,this.getHeight()-1));
		
		this.getGrille()[this.getWidth()-1][0] = pList.get(1);
		pList.get(1).getCellList().add(new Pair<Integer, Integer>(this.getWidth()-1,0));
		
		if(pList.size() == 3){
			this.getGrille()[(int)Math.ceil(this.getWidth()/2)][(int)Math.ceil(this.getHeight()/2)] = pList.get(2);
			pList.get(2).getCellList().add(new Pair<Integer, Integer>((int)Math.ceil(this.getWidth()/2),(int)Math.ceil(this.getHeight()/2)));
		}
		
		else if(pList.size() == 4){
			this.getGrille()[0][0] = pList.get(2);
			pList.get(2).getCellList().add(new Pair<Integer, Integer>(0,0));
			
			this.getGrille()[this.getWidth()-1][this.getHeight()-1] = pList.get(3);
			pList.get(3).getCellList().add(new Pair<Integer, Integer>(this.getWidth()-1,this.getHeight()-1));
		}
		
	}

}
