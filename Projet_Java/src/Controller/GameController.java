package Controller;

import java.util.ArrayList;

import Model.Computer;
import Model.Game;
import Model.Player;
import Utils.MapAnalyze;
import View.EndGameView;
import View.GameView;
import javafx.application.Platform;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Pair;

public class GameController {

	private Game game;
	private Stage stg;
	
	public GameController(Stage s, Game g){
		this.stg= s;
		this.game=g;
	}
	
	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}
	
	public Stage getStage(){
		return this.stg;
	}
	
	public void launch(){
		for(Player p : this.game.getPlayers()){
			this.updateMap(p ,p.getColor(), this.game.getMap().getGrille(), 0);
		}
		
		GameView gv = new GameView(this);
		this.game.addObserver(gv);
		gv.initUI(this.stg);
	}
	
	
	
	
	// Update game data when a player chose a color
	public void play(int nbPlayer, int color){
		
		// update player color
		Player p = this.getGame().getPlayers().get(nbPlayer);
		p.setColor(Game.getColors().get(color));
		
		// update Map
		this.updateMap(p, p.getColor(), this.game.getMap().getGrille(), 0);
		
		
		// Check if a player wins
		if(this.getWinner() != null){
			new EndGameView(this).initUI(this.stg);
		}
		
		else{
			// Change current player
			this.game.setCurrent(this.game.getPlayers().get((nbPlayer+1)%this.game.getPlayers().size()));
			
			
			
			if((this.game.getCurrent() instanceof Computer))
			{
				Computer c= (Computer)(this.game.getCurrent());
				int col =c.play();
				
				GameView.incrementCursor(this.game.getPlayers().size());
				this.play(GameView.getCursor(), col);
			}
		}
	}
	
	// update Map
	public void updateMap(Player p, Color c , Object[][] g, int indice){
		MapAnalyze mapAn = new MapAnalyze(this.game.getMap());
		Object[][] grille = g;
		Pair<Integer, Integer> cell;
		int nbChange = 0;
		
		
		
		if(p.getCellList().isEmpty() || indice==p.getCellList().size()){
			return;
		}
		
		else{
			cell=p.getCellList().get(indice);
		}
		
		
		if(mapAn.checkRightCellExist(cell, grille.length) 
				&& grille[cell.getKey()+1][cell.getValue()] !=null
				&& Game.getColors().get(grille[cell.getKey()+1][cell.getValue()]) == c){
			
			grille[cell.getKey()+1][cell.getValue()] = p;
			
			p.getCellList().add(new Pair<Integer,Integer>(cell.getKey()+1,cell.getValue()));
			p.addScore(1);
			++nbChange;
		}
		
		if(mapAn.checkLeftCellExist(cell, grille.length) 
				&& grille[cell.getKey()-1][cell.getValue()] !=null
				&& Game.getColors().get(grille[cell.getKey()-1][cell.getValue()]) == c){
			
			grille[cell.getKey()-1][cell.getValue()] = p;
			
			p.getCellList().add(new Pair<Integer,Integer>(cell.getKey()-1,cell.getValue()));
			p.addScore(1);
			++nbChange;
		}

		if(mapAn.checkTopCellExist(cell, grille[0].length)
				&& grille[cell.getKey()][cell.getValue()-1] !=null
				&& Game.getColors().get(grille[cell.getKey()][cell.getValue()-1]) == c){
			
			grille[cell.getKey()][cell.getValue()-1] = p;
			
			p.getCellList().add(new Pair<Integer,Integer>(cell.getKey(),cell.getValue()-1));
			p.addScore(1);
			++nbChange;
		}
		
		if(mapAn.checkBottomCellExist(cell, grille[0].length)
				&& grille[cell.getKey()][cell.getValue()+1] !=null
				&& Game.getColors().get(grille[cell.getKey()][cell.getValue()+1]) == c){
			
			grille[cell.getKey()][cell.getValue()+1] = p;
			
			p.getCellList().add(new Pair<Integer,Integer>(cell.getKey(),cell.getValue()+1));
			p.addScore(1);
			++nbChange;
		}
		
		
		if(nbChange==4){
			p.getCellList().remove(indice);
		}
		
		else{
			indice++;
		}
		
		this.updateMap(p, p.getColor(), grille, indice);
	}
	
	
	
	
	// Check end of the game
	public ArrayList<Player> getWinner(){
		
		if(this.game.getCurrent().getScore() >= (this.game.getMap().getNbCell())){
			ArrayList<Player> bestPlayer = new ArrayList<Player>();
			bestPlayer.add(this.game.getCurrent());
		}
		
		else if(this.game.isEndOfGame()){
			return this.game.getBestPlayer();
		}
		
		return null;
	}
	
	
	/*
	 *  GAME MENU BAR LISTENER
	 */
	
	
	// Return to the main menu
	public void newGame(){
		this.game=null;
		new MenuController(this.stg).launchMenu();
	}
	
	// Save the game in txt file
	public void save(){
		System.out.println("Saving Game...");
	}
	
	// Quit the application
	public void quit(){
		Platform.exit();
		System.exit(0);
	}
	
}
