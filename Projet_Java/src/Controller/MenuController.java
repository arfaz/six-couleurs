package Controller;

import java.util.ArrayList;

import Model.Game;
import Model.Player;
import Model.Map.Map;
import Pattern.MapFactory;
import Pattern.PlayerFactory;
import View.ComputerMenu;
import View.LevelMenu;
import View.MapMenu;
import View.PlayerMenu;
import javafx.stage.Stage;

public class MenuController {

	private Game game;
	private Stage stg;
	
	private ArrayList<Boolean> playersType = new ArrayList<Boolean>();
	
	// Constructor
	public MenuController(Stage s){
		this.game = new Game();
		this.stg=s;
	}
	
	// Launch the player menu
	public void launchMenu(){
		new PlayerMenu(this).initUI(this.stg);
	}
	
	// Create computer player
	public void nbComputerChoice(int nbHumanPlayers){
		
		for(int i=0; i<nbHumanPlayers; i++){
			this.playersType.add(true);
		}
		
		// if number of player < 4 , launch the computer Menu
		if(nbHumanPlayers<4){
			new ComputerMenu(this,nbHumanPlayers).initUI(this.stg);
		}
		
		// else, call the method which create players
		else{
			this.createPlayers(0);
		}
	}	
	
	
	//  Create players (human and computer)
	public void createPlayers(int nbComputer){
		
		for(int i=0; i<nbComputer; i++){
			this.playersType.add(false);
		}
		
		
		if(nbComputer>0){
			// Launch the Computer Level Menu
			new LevelMenu(this).initUI(this.stg);
		}
		
		else{
			// Launch the Map Shape Choice Menu
			new MapMenu(this).initUI(this.stg);
		}
	}
	
	public void levelChoice(int lvl){
		this.game.setLevel(lvl);
				
		// Launch the Map Shape Choice Menu
		new MapMenu(this).initUI(this.stg);
	}
	
	public void createMap(String type){
		
		Map m = MapFactory.getInstance().createMap(type);
		
		// Set the map to the game
		this.game.setMap(m);
		
		// Call the Player Factory
		ArrayList<Player> playerList = PlayerFactory.getInstance().createPlayers(this.playersType, this.game.getLevel(), this.game);
		this.game.setPlayer(playerList);
		this.game.setCurrent(this.game.getPlayers().get(0));
				
		// Init player position in the map
		this.game.getMap().initPlayerPosition(this.game.getPlayers());
		
		// Create obstacles
		this.game.getMap().createObstacle(this.game.getLevel());
		
		// Launch the game
		new GameController(this.stg, this.game).launch();
	}
}
