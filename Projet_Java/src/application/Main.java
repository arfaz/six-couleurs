package application;
import Controller.MenuController;
import javafx.application.Application;
import javafx.stage.Stage;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
		
			primaryStage.setResizable(false);
			
			// Call the game creation Menu
			MenuController mc = new MenuController(primaryStage);
			mc.launchMenu();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
