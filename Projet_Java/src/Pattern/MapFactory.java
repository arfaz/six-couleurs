package Pattern;

import Model.Map.DiamondMap;
import Model.Map.HexaMap;
import Model.Map.Map;
import Model.Map.SquareMap;
import Model.Map.TriangleMap;

public class MapFactory {

	private static MapFactory mf = null;
	
	private MapFactory(){}
	
	public static MapFactory getInstance(){
		if(mf == null){
			mf = new MapFactory();
		}
		
		return mf;
		
	}
	
	public Map createMap(String type){
		Map m=null;
		
		switch(type){
		
		case "square":
			m = new SquareMap();
			break;
		
		case "diamond":
			m = new DiamondMap();
			break;
		
		case "triangle":
			m = new TriangleMap();
			break;
			
		case "hexa":
			m = new HexaMap();
			break;
		}
		
		return m;
	}
	 
}
