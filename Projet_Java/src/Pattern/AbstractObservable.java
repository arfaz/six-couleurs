package Pattern;

import java.util.ArrayList;

public abstract class AbstractObservable implements Observable {

	private ArrayList<Observer> observers;
	
	public AbstractObservable(){
		this.observers= new ArrayList<Observer>();
	}
	
	@Override
	public void addObserver(Observer obs) {
		this.observers.add(obs);
	}

	@Override
	public void removeObserver() {
		this.observers= new ArrayList<Observer>();
	}

	@Override
	public void notifyObserver() {
		for(Observer obs : this.observers){
			obs.update();
		}
	}
}
