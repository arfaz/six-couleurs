package Pattern;

import java.util.ArrayList;
import java.util.Random;

import Model.ComputerEasy;
import Model.ComputerHard;
import Model.ComputerMedium;
import Model.Game;
import Model.Player;
import Utils.MapAnalyze;
import javafx.scene.paint.Color;

public class PlayerFactory {

	private static PlayerFactory pf = null;
	
	private PlayerFactory(){}
	
	public static PlayerFactory getInstance(){
		if(pf == null){
			pf = new PlayerFactory();
		}
		return pf;
	}
	
	public ArrayList<Player> createPlayers(ArrayList<Boolean> playersTypeList, int computerLevel, Game g){
		ArrayList<Color> unavailableColors = new ArrayList<Color>();
		ArrayList<Player> playerList = new ArrayList<Player>();
		
		Random r = new Random();

		for(int i =0; i< playersTypeList.size(); i++){
			
			Color c = (Color)Game.getColors().get(r.nextInt(6) +1);
			
			// Generate a new random color while it don't already exist
			while(unavailableColors.contains(c)){
				c = (Color)Game.getColors().get(r.nextInt(6) +1);
			}
			
			// Add this color to a list of used color
			unavailableColors.add(c);
			
			Player p = null;
			if(playersTypeList.get(i)){
				p = new Player("Player"+(i+1), c);
				p.setShortName(i+1+"");
			}
			
			else{
				
				switch(computerLevel){
					case 1:
						p = new ComputerEasy((i+1), c, g, new MapAnalyze(g.getMap()));
				
					case 2:
						p = new ComputerMedium((i+1), c, g, new MapAnalyze(g.getMap()));
				
					case 3:
						p = new ComputerHard((i+1), c, g, new MapAnalyze(g.getMap()));
				}
			
				p.setShortName(i+1+"");
			}	
			playerList.add(p);
		}
		return playerList;
	}
}

